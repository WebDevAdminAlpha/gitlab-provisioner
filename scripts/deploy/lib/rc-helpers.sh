# shellcheck shell=bash

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/src-paths.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

select_ssh_key() {
    local key_name="${1}"

    key_name="${key_name:-gitlab-orchestrator}"
    # The priority order to load a key is:
    # - Directly Mounted Key
    local mount_key_path="${SRC_PATH}/${MOUNTED_SSH_KEY_DIR}/${key_name}"
    # - Key in the mounted host key directory
    local host_key_path="${SRC_PATH}/${HOST_SSH_KEY_DIR}/${key_name}"
    # - Dynamically generated key
    local dynamic_key_path="${HOME}/.ssh/${key_name}"

    local ssh_key
    if [ -f "${mount_key_path}" ]; then
        ssh_key="${mount_key_path}"
    elif [ -f "${host_key_path}" ]; then
        ssh_key="${host_key_path}"
    else
        ssh_key="${dynamic_key_path}"

        if [ ! -f "${ssh_key}" ]; then
            ssh_dir="$(dirname "${ssh_key}")"
            mkdir -p "${ssh_dir}"
            chmod 0700 "${ssh_dir}"
            ssh-keygen -b 4096 -t rsa -f "${ssh_key}" -q -N ""
        fi
    fi

    echo "${ssh_key}"
}

validate_package_source() {
    local package_url="${1}"
    local package_repository="${2}"
    local package_repository_url="${3}"
    local gl_token="${4}"

    if [ -n "${gl_token}" ]; then
        [ -n "${package_url}" ] || fail "Use of GitLab Tokens requires a package url"
    fi

    if [ -n "${package_url}" ]; then
        if [ -n "${package_repository}" ] || [ -n "${package_repository_url}" ]; then
            fail "Cannot use both a repository and a package url"
        fi
    fi
}
