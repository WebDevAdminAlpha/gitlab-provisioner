#!/bin/bash

set -e

src_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../ >/dev/null 2>&1 && pwd )"
[ -z "${SRC_PATH}" ] && export SRC_PATH="${src_path}"

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/opt-functions.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

usage() {
    name=$(basename "$0")
    echo "Usage:"
    echo "${name} [-c] PATH"
    echo ""
    echo "Assembles inventory information prior to orchestration"
    echo ""
    echo "    -h    Display help message"
    echo ""
    echo "    -c    Path to a configuration file defining the settings for"
    echo "          identifying inventory for a GitLab Cluster in Google Cloud"
}

while getopts ":hc:" opt; do
    case "${opt}" in
        h )
            usage
            exit 1
            ;;
        c )
            file_exists "${OPTARG}"
            GITLAB_CLUSTER_RC="$(portable_readlink "${OPTARG}")"
            export GITLAB_CLUSTER_RC
            ;;
        \? )
            opt_invalid "${OPTARG}" usage
            ;;
        : )
            opt_requires_arg "${OPTARG}" usage
            ;;
    esac
done

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/orchestrator_env.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/ansible_common.sh"

ANSIBLE_INVENTORY_TEMPLATE="${ANSIBLE_ASSETS}/inventory/base.gcp.yml.erb"

if [ -f "${ANSIBLE_INVENTORY_FILE}" ]; then
    rm "${ANSIBLE_INVENTORY_FILE}"
fi

erb project="${GCLOUD_PROJECT}" \
    service_account_file="${GOOGLE_APPLICATION_CREDENTIALS}" \
    host_prefix="${TERRAFORM_PREFIX}-*" \
    "${ANSIBLE_INVENTORY_TEMPLATE}" >> "${ANSIBLE_INVENTORY_FILE}"

ANSIBLE_DATA=$(ansible-inventory -i "${ANSIBLE_INVENTORY_FILE}" --list)

echo "${ANSIBLE_DATA}" | jq --arg package_url "${PACKAGE_URL}" --arg package_repository "${PACKAGE_REPOSITORY}" \
    --arg package_repository_url "${PACKAGE_REPOSITORY_URL}" \
    --arg ssh_user "${ORCHESTRATION_USER}" --arg become_method "${ANSIBLE_BECOME_METHOD}" '
    [._meta.hostvars[].networkInterfaces[].networkIP + "/32"] as $cidrs |
        {
            ansible_user: $ssh_user,
            ansible_become_method: $become_method,
            private_token: env.GL_PRIVATE_TOKEN,
            expected_master: (env.TERRAFORM_PREFIX + "-database-0"),
            postgresql_version: env.postgresql_version,
            postgresql_gitlab_consul_user_password: env.postgresql_gitlab_consul_user_password,
            postgresql_pgbouncer_user_password: env.postgresql_pgbouncer_user_password,
            postgresql_sql_user_password: env.postgresql_sql_user_password,
            repmgr_trust_auth_cidr_addresses: $cidrs,
            postgresql_trust_auth_cidr_addresses: $cidrs,
            postgresql_listen_address: "0.0.0.0",
        } |
            . + if $package_url != "" then {package_url: $package_url} else null end |
            . + if $package_repository != "" then {package_repository: $package_repository} else null end |
            . + if $package_repository_url != "" then {package_repository_url: $package_repository_url} else null end
                    ' > "${ANSIBLE_EXTRA_VARS_FILE}"

mapfile -t ip_addresses < <(echo "${ANSIBLE_DATA}" |  jq -r '._meta.hostvars[].networkInterfaces[].accessConfigs[].natIP')
# Configurable Sleep Allowing GCP Provisioning to complete
sleep "${KEYSCAN_WAIT_SECONDS}"
ssh-keyscan -t "${SSH_KEY_TYPE}" "${ip_addresses[@]}" >> "${SSH_KNOWN_HOSTS_FILE}"
chmod 644 "${SSH_KNOWN_HOSTS_FILE}"
